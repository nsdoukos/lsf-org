# Main organizational project for Libre Space Foundation #

In this GitLab project you can find issues that LSF uses to track high level non-repo-specific work.
This repository also contains the source code for documentation of Libre Space Foundation.


## Styling guide ##

reStructeredText in this repository shall follow this styling guide for source code consistency.
These rules **aren't** checked as part of quality gating with a CI job.


### Sections ###

The succession of headings shall follow the [Python Developer's Guide for documentation](https://devguide.python.org/documentation/markup/#sections):

<!-- vale Google.Units = NO -->
  * 1st level, `#` with overline
  * 2nd level, `*` with overline
  * 3rd level, `=`
  * 4th level, `-`
  * 5th level, `^`
  * 6th level, `"`
<!-- vale Google.Units = YES -->

### Lists ###

Numbered lists shall be auto-numbered using the `#.` sign, unless required otherwise.
Unnumbered lists shall only use `*` sign, unless required otherwise.


### Paragraphs ###

All sentences shall end with a punctuation mark followed by a newline character.
Wrap the lines to 79 characters as in GNU formatting style.


## Linting ##

To lint the source code of the documentation, run:

```
$ tox run-parallel -e doc8,sphinx-lint,vale,linkcheck
```


## Building ##

To build the documentation, run:

```
$ tox run -e docs
```

To re-build in a re-created environment, run:

```
$ tox run -r -e docs
```


### Sphinx-autobuild ###

To automatically build documentation on any change and access it through a local web server, run:

```
$ docker-compose up --build
```

By default, a server starts listening on http://localhost:8000 , serving the rendered HTML documents.


## Maintenance ##

Package dependencies for building the documentation are in:

  * `requirements.txt` - Dependencies to build the docs
  * `requirements-dev.txt` - Packages used for development
  * `constraints.txt` - Package version constraints
  * `packages.alpine` - Alpine packages required to build docs

To update the dependencies, edit the version ranges in `requirements.txt` and `requirements-dev.txt` files, and then run `contrib/refresh-requirements-docker.sh` to regenerate the `constraints.txt` file.
