##########
Facilities
##########

This page outlines the guidelines and procedures for managing the Libre Space Foundation's
facilities, ensuring a safe, functional, and productive environment for all members, staff,
and visitors.


Access to facilities
====================

* **Membership**: Access to LSF facilities is generally granted to all core contributors.
* **Guests**: Guests must be accompanied by a core contributor at all times. People are responsible for the conduct of their guests.
* **Access Control**:

 * Specific areas (for example the clean room) may require keycard access or other security measures.
 * People must familiarize themselves with access procedures for their designated areas.

.. note::
    For access to the Athens HQ you need physical keys and an alarm code.
    Open an issue in the `Facilities Management GitLab project <https://gitlab.com/librespacefoundation/lsf-fm-org>`_ to request them.
    Physical keys are tracked in `IvenTree as a part <https://inventory.hackerspace.gr/part/1122/>`_.


* **Hours of Operation**: Standard operating hours for LSF facilities are 09:00 to 17:00 local. Access outside these hours may be possible upon request and coordination.

Security
========

* **General Security**:

 * Members are responsible for maintaining the security of the facilities.
 * Report any suspicious activity or security breaches immediately to the Facilities Manager.
 * Do not prop open doors or allow unauthorized access.
 * Be aware of emergency exits and procedures.

* **Alarms**: Familiarize yourself with the location and operation of fire alarms and security systems.

Resources
=========

* **Network Access**: All facilities have Wi-Fi access open to all. Access details are available next to the entrance of each facility.
* **Software**: Use of licensed software must comply with licensing agreements. Do not install unauthorized software.
* **Issues**: If you have any LSF Ops related issues `Open an issue in the support GitLab project <https://gitlab.com/librespacefoundation/ops/support/-/issues/new>`_.

 .. warning::
    If the issue you are facing is specific to the Athens HQ (hackerspace.gr) then please
    `open an issue to the hsgr/ops/support GitLab project <https://gitlab.com/hsgr/ops/support/-/boards>`_
    since this is managed by a volunteer team at Hackerspace.gr. Examples are: intermittent network connection, smart automation etc

Food and drinks
===============

* **Designated Areas**: Consume food and drinks only in designated areas (for example kitchen, front room).
* **Cleanliness**: Clean up after yourself and dispose of waste properly.
* **Refrigerator**: Label personal food items stored in the refrigerator with your name and date. Dispose of expired items promptly.

General facility etiquette
==========================

* **Cleanliness**: Maintain a clean and organized workspace. Dispose of waste properly and recycle whenever possible.
* **Noise Levels**: Be mindful of noise levels, especially in shared workspaces.
* **Respect for Others**: Treat fellow members, staff, and visitors with respect.
* **Equipment Use**: Return shared equipment to its designated location after use, clean and in good working order.
* **Safety**: Adhere to all safety guidelines and regulations. Report any safety hazards to the Facilities Manager.

Reporting issues and maintenance
================================

* **Facilities Manager**: Aris Nikas is the designated Facilities Manager. Nestoras Sdoukos is the project Champion.
* **Reporting Issues**: Report any facility-related issues (for example, maintenance requests, cleaning needs, security concerns) by sending an email to facilities@libre.space .
* **Maintenance**: Routine maintenance is scheduled regularly. Members should report any urgent maintenance needs promptly.
